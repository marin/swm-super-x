
(in-package :super-x)

(defvar *text-style* (make-text-style :sans-serif :roman 36))

(define-application-frame super-x ()
  ((return-val :initform nil :accessor frame-return-value))
  (:menu-bar nil)
  (:top-level (default-frame-top-level :prompt 'super-x-prompt))
  (:panes (int :interactor
               :width 800
               :height 400
               :text-style *text-style*))
  (:layouts
   (default int)))

(defun run-super-x ()
  (let ((frame (make-application-frame 'super-x)))
    (run-frame-top-level frame)
    (apply 'values (frame-return-value frame))))

(defun %exit-super-x-frame (&optional value)
  (setf (frame-return-value *application-frame*) value)
  (frame-exit *application-frame*))

(defmacro exit-super-x (form)
  `(%exit-super-x-frame (multiple-value-list ,form)))

(defun super-x-prompt (pane frame)
  (declare (ignore frame))
  (with-text-style (pane *text-style*)
    (stream-increment-cursor-position pane 3 4)
    (surrounding-output-with-border (pane :shape :drop-shadow :move-cursor nil)
      (format pane "~%SWM Command: "))
    (stream-increment-cursor-position pane 20 0)))

;;; So, we want to translate the stumpwm commands into clim commands. They
;;; should be easy enough /AFTER/ we have manually translated the stumpwm types
;;; to clim presentation types with accept methods.

(defun translate-swm-type (type token)
  (values 
   (catch 'stumpwm::error
     (return-from translate-swm-type
       (values (funcall (gethash type stumpwm::*command-type-hash*)
                        (stumpwm::make-argument-line :string token :start 0)
                        "")
               t)))
   nil))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defvar *swm-type-to-clim-type* (make-hash-table))

  (defun find-translated-type-symbol (sym)
    (gethash sym *swm-type-to-clim-type*))
  
  (defun make-com-name (cmd)
    (intern (string-upcase (concatenate 'string
                                        "com-"
                                        (string (stumpwm::command-name cmd))))))
  
  (defun make-com-args (cmd)
    (mapcar (lambda (arg)
              (if (listp arg)
                  `(,(gensym (string (car arg)))
                    ',(find-translated-type-symbol (car arg))
                    :prompt
                    ,(cadr arg))
                  `(,(gensym (string arg))
                    ',(find-translated-type-symbol arg))))
            (stumpwm::command-args cmd)))

  (defun make-call-name (cmd)
    (string-downcase (string (stumpwm::command-name cmd)))))

(defun def-stype-trans (swm-type clim-type
                        &optional custom
                        &rest presentation-type-keys)
  (setf (gethash swm-type *swm-type-to-clim-type*) clim-type)
  (eval `(define-presentation-method accept ((type ,clim-type) stream view &key)
           ,(if custom
                `(,custom type stream view)
                `(flet ((invalid (val tok)
                          (format t "~%~%~A~%~%" val)
                          (input-not-of-required-type tok type)))
                   (let ((tok (read-token stream)))
                     (multiple-value-bind (val valid)
                         (handler-case (translate-swm-type ,swm-type tok)
                           (error (c)
                             (invalid c tok)))
                       (when valid
                         (return-from accept val))
                       (invalid val tok))))))))

(defun do-type-translation (&optional (hash stumpwm::*command-type-hash*))
  (maphash (lambda (k v)
             (declare (ignore v))
             (def-stype-trans k (intern (string k))))
           hash))

;; (do-type-translation)

(defun def-scommand-trans (command-name swm-command)
  (flet ((make-command-name ()
           (intern
            (string-upcase (concatenate 'string "COM-" (string command-name)))))
         (make-calling-name ()
           (string-downcase (string command-name))))
    (let ((args (make-com-args swm-command)))
      (eval
       `(define-super-x-command
            (,(make-command-name) :name ,(make-calling-name))
          ,args
          (let ((rc (stumpwm::get-command-structure ',command-name)))
            (if rc
                (exit-super-x (funcall ',(stumpwm::command-name swm-command)
                                       ,@(mapcar #'car args)))
                (format t ,(concatenate 'string
                                        "~%Command "
                                        (string command-name)
                                        " is not active~%")))))))))

(defun get-command-for-alias (alias)
  (let ((command (stumpwm::get-command-structure
                  (stumpwm::command-alias-from alias))))
    (if (stumpwm::command-alias-p command)
        (get-command-for-alias command)
        command)))

(defun do-command-translation (&optional (hash stumpwm::*command-hash*))
  (maphash (lambda (k v)
             (if (stumpwm::command-alias-p v)
                 (def-scommand-trans k (get-command-for-alias v))
                 (def-scommand-trans k v)))
           hash))

(defmacro define-swm-type-translator (swm-type clim-type
                                      &optional custom
                                      &rest keys)
  `(progn
     ;; We have to define the presentation type here. 
     (define-presentation-type ,clim-type () ,@keys)
     (def-stype-trans ,swm-type ',clim-type ,custom ,@keys)))

(defmacro define-swm-command-translator (command-name)
  `(def-scommand-trans ',command-name
                       (stumpwm::get-command-structure ',command-name)))

(define-swm-type-translator :y-or-n y-or-n)
(define-swm-type-translator :variable variable)
(define-swm-type-translator :function function)
(define-swm-type-translator :command command)
(define-swm-type-translator :key-seq key-seq)
(define-swm-type-translator :window-number window-number)
;; (define-swm-type-translator :number number)
(setf (gethash :number *swm-type-to-clim-type*) 'number)

;; string types are already implemented
(setf (gethash :string *swm-type-to-clim-type*) 'string)

;; Password types take some extra work. 
(defun accept-password (type stream view)
  (declare (ignore type view))
  (return-from accept-password
    (with-output-recording-options (stream :draw nil :record nil)
      (read-token stream))))

(define-swm-type-translator :password password 'accept-password
  :inherit-from '((string)) :description "Password")

(define-presentation-method present
    (password (type password) stream (view textual-view) &key acceptably)
  (when acceptably (error "Not acceptably"))
  (write-string (make-string (length password) :initial-element #\*) stream))

(define-swm-type-translator :key key)
(define-swm-type-translator :window-name window-name)
(define-swm-type-translator :direction direction)
(define-swm-type-translator :gravity gravity)
(define-swm-type-translator :group group)
(define-swm-type-translator :frame frame)
(define-swm-type-translator :shell shell)
(define-swm-type-translator :rest rest)

(do-command-translation)

(define-presentation-type window ())
(define-presentation-method present (window (type window) stream view &key)
  (write-string (stumpwm::window-name window) stream))
(define-presentation-method accept ((type window) stream view &key)
  (values
   (completing-from-suggestions (stream :partial-completers '(#\space))
     (mapcar (lambda (win)
               (suggest (stumpwm::window-name win) win))
             (stumpwm:group-windows (stumpwm:current-group))))))

(define-super-x-command (com-read-password :name "read-password")
    ((pass 'password :prompt "password: "))
  (exit-super-x (values (accept 'winname)
                        pass)))

(define-super-x-command (com-read-window :name "read-window")
    ((win 'window :prompt "Window Name: "))
  (exit-super-x win))
