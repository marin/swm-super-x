(asdf:defsystem #:super-x
  :name "An emacs-esq M-x in CLIM"
  :depends-on (#:stumpwm
               #:m-util
               #:clim
               #:mcclim)
  :components ((:file "package")
               (:file "frame")))
