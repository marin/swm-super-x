# SUPER-X

This is a system to reimplement the `colon` stumpwm command in CLIM. The most
significant portions of this are the macros `define-swm-type-translator` and
`define-swm-command-translator`. These translate a stumpwm type and command to a
clim type and command. This system includes a basic frame which implements
`colon`.

This system must be loaded **after** stumpwm, and must be recompiled every time,
as the macros look up the command structures at macroexpansion time. These
structures are needed at macroexpansion time in order to generate the correct
command name and argument list. 